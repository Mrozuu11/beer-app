import { Beer } from "../services/beer/responses/beers";
import BeerTile from "./BeerTile";
import Button from "./shared/Button";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";

interface PaginatedBeersProps {
  beers: Beer[];
  pageNumber: number;
  decreasePageNumber: () => void;
  increasePageNumber: () => void;
  onEnter: () => void;
  isFetching: boolean;
}
export default function PaginatedBeers(
  props: PaginatedBeersProps
): JSX.Element {
  const {
    beers,
    pageNumber,
    decreasePageNumber,
    increasePageNumber,
    onEnter,
    isFetching,
  } = props;

  return (
    <>
      <div className="flex items-center justify-center flex-wrap padding-x-main py-8 lg:py-12 gap-8">
        {beers.map((beer) => (
          <BeerTile beer={beer} key={beer.id} />
        ))}
      </div>
      <div className="p-4 flex justify-center gap-2">
        <Button
          handleClick={decreasePageNumber}
          onEnter={onEnter}
          disabled={pageNumber === 1}
          isLoading={isFetching}
        >
          <KeyboardArrowLeftIcon />
        </Button>
        <div className="w-[40px] p-2 text-white bg-orange font-bold text-center rounded-md transition duration-300">
          {pageNumber}
        </div>
        <Button
          handleClick={increasePageNumber}
          onEnter={onEnter}
          disabled={pageNumber === 28}
          isLoading={isFetching}
        >
          <KeyboardArrowRightIcon />
        </Button>
      </div>
    </>
  );
}
