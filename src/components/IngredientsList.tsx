import { Ingredient } from "../services/beer/responses/beers";

interface IngredientsListProps {
  ingredientType: Ingredient[];
  imgSrc: string;
  name: string;
}
export default function IngredientsList(
  props: IngredientsListProps
): JSX.Element {
  const { ingredientType, imgSrc, name } = props;
  return (
    <div>
      <div className="flex gap-2 mb-4">
        <img src={imgSrc} alt={imgSrc} className="w-5" />
        <h3 className="text-orange">{name}</h3>
      </div>

      {ingredientType.map((item, index) => (
        <ul key={index} className="flex gap-2 ">
          <li className="flex gap-2 ft-regular">
            &#x2022; <span>{item.name}</span>
            {item.add && (
              <span>
                &#x2022;<span className="ml-2">{item.add}</span>
              </span>
            )}
            {item.attribute && (
              <span>
                &#x2022;<span className="ml-2">{item.attribute}</span>
              </span>
            )}
          </li>
        </ul>
      ))}
    </div>
  );
}
