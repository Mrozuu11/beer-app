import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import { Beer } from "../services/beer/responses/beers";
import { useNavigate } from "react-router-dom";

interface HeaderProps {
  beer: Beer;
}
export default function DetailsHeader(props: HeaderProps): JSX.Element {
  const { beer } = props;
  const navigate = useNavigate();
  const navigateBack = () => navigate(-1);

  return (
    <header className="bg-indigo-main p-5 md:p-10 pb-12">
      <button
        type="button"
        onClick={navigateBack}
        className="flex items-center font-bold text-white"
      >
        <KeyboardArrowLeftIcon />
        <span>Home</span>
      </button>
      <div className="h-full flex items-center justify-center mt-4">
        <h1 className="text-orange leading-snug">{beer.name}</h1>
      </div>
    </header>
  );
}
