import { ReactNode } from "react";
import Loader from "./Loader";

interface ButtonProps {
  handleClick: () => void;
  onEnter: () => void;
  children: ReactNode;
  disabled: boolean;
  isLoading: boolean;
}

export default function Button(props: ButtonProps): JSX.Element {
  const { handleClick, onEnter, children, disabled, isLoading } = props;
  if (isLoading) return <Loader variant="small" />;
  return (
    <button
      disabled={disabled}
      onClick={handleClick}
      onMouseEnter={onEnter}
      className="hover:scale-125 transition duration-300
      disabled:opacity-50 disabled:hover:scale-100"
    >
      {children}
    </button>
  );
}
