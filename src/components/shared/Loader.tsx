interface LoaderProps {
  variant?: string;
}
export default function Loader(props: LoaderProps): JSX.Element {
  const { variant } = props;
  return (
    <div
      className={`${!variant && "fixed top-0 left-0 h-full w-full bg-slate-300"}
      grid justify-items-center content-center`}
    >
      <div
        className={`${
          variant === "small" ? "w-1 h-1 loader-small" : "w-36 h-36 loader"
        } p-2 rounded-full transition animate-spin`}
      ></div>
    </div>
  );
}
