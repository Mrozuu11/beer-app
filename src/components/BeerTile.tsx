import { Link } from "react-router-dom";
import { Beer } from "../services/beer/responses/beers";
import fallbackBottle from "../assets/fallback-bottle.jpg";

interface BeerTileProps {
  beer: Beer;
}
export default function BeerTile(props: BeerTileProps): JSX.Element {
  const { beer } = props;
  const imgSrc = beer.image_url !== null ? beer.image_url : fallbackBottle;
  return (
    <Link
      to={`/details/${beer.id}`}
      className="w-64 h-[400px] flex flex-col items-center justify-center gap-2 p-8 rounded-md shadow-lg text-center bg-slate-50"
    >
      <div className="">
        <img src={imgSrc} alt={beer.name} className="max-h-60" />
      </div>
      <div className="w-full ft-regular font-bold truncate border-b-zinc-300 border-b-[1px] text-indigo-main">
        {beer.name}
      </div>
      <div className="ft-small">{beer.tagline}</div>
    </Link>
  );
}
