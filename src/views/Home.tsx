import { useCallback, useState } from "react";
import Loader from "../components/shared/Loader";
import { usePrefetch, useGetBeersQuery } from "../services";
import PaginatedBeers from "../components/PaginatedBeers";
import { useNavigate } from "react-router-dom";

export default function Home(): JSX.Element {
  //paginated navigation adapted from:
  //https://stackoverflow.com/questions/66730490/react-router-pagination-with-query-string
  const history = useNavigate();
  let search = window.location.search;
  let page = new URLSearchParams(search).get("page");
  const [pageNumber, setPageNumber] = useState<number>(parseInt("" + page));
  const increasePageNumber = () => {
    setPageNumber(pageNumber + 1);
    history({
      pathname: "/beers",
      search: `?page=${pageNumber + 1}`,
    });
  };
  const decreasePageNumber = () => {
    pageNumber > 1 && setPageNumber(pageNumber - 1);
    history({
      pathname: "/beers",
      search: `?page=${pageNumber - 1}`,
    });
  };
  const prefetchPage = usePrefetch("getBeers");
  const prefetchNext = useCallback(() => {
    prefetchPage(pageNumber + 1);
  }, [prefetchPage, pageNumber]);
  const {
    data: beers,
    error,
    isLoading,
    isFetching,
  } = useGetBeersQuery(pageNumber);
  if (isLoading) return <Loader />;
  if (error) return <div>Error</div>;
  if (beers)
    return (
      <main>
        <h1 className="padding-x-main p-10 font-bold text-center text-orange bg-indigo-main">
          Beers
        </h1>
        <PaginatedBeers
          beers={beers}
          pageNumber={pageNumber}
          decreasePageNumber={decreasePageNumber}
          increasePageNumber={increasePageNumber}
          onEnter={prefetchNext}
          isFetching={isFetching}
        />
      </main>
    );
  return <></>;
}
