import { useParams } from "react-router-dom";
import fallbackBottle from "../assets/fallback-bottle.jpg";
import hop from "../assets/hop.png";
import malt from "../assets/malt.png";
import yeast from "../assets/yeast.png";
import IngredientsList from "../components/IngredientsList";
import Loader from "../components/shared/Loader";
import { useGetBeerByIdQuery } from "../services";
import { Beer } from "../services/beer/responses/beers";
import DetailsHeader from "../components/DetailsHeader";

export default function BeerDetails(): JSX.Element {
  const { beerId } = useParams();
  const {
    data: beers,
    isLoading,
    error,
  } = useGetBeerByIdQuery(beerId as string);
  const imgSrc = (beer: Beer) =>
    beer.image_url !== null ? beer.image_url : fallbackBottle;

  if (isLoading) return <Loader />;
  if (error) return <div>Error</div>;
  if (beers)
    return (
      <>
        {beers.map((beer) => (
          <div key={beer.id} className="h-screen lg:flex">
            <DetailsHeader beer={beer} />
            <main className="padding-x-main py-16 flex flex-col items-center justify-center ">
              <h2 className="mb-12">{beer.tagline}</h2>
              <div className="flex gap-16 flex-wrap lg:flex-nowrap items-center justify-center">
                <div
                  id="img-container"
                  className="flex items-center justify-center"
                >
                  <img
                    src={imgSrc(beer)}
                    alt={beer.name}
                    className="max-w-xs max-h-[600px] object-contain"
                  />
                </div>
                <div id="text-container">
                  <div
                    id="desc-container"
                    className="flex justify-between flex-wrap mb-3 gap-3"
                  >
                    <div className="p-5 lg:basis-7/12 border-2 rounded-md shadow-md bg-slate-100">
                      <div className="py-4">
                        <h3 className="font-bold mb-4 text-indigo-main">
                          Description:
                        </h3>
                        <p className="ft-regular">{beer.description}</p>
                      </div>
                      <h3 className="text-left pt-4 font-bold">
                        First Brewed: {beer.first_brewed}
                      </h3>
                    </div>
                    <div className="w-full flex flex-col justify-between p-5 lg:basis-2/5 border-2 rounded-md shadow-md bg-slate-100">
                      <div className="py-4">
                        <h3 className="font-bold mb-4 text-indigo-main">
                          Food Pairing
                        </h3>
                        <ul>
                          {beer.food_pairing.map((dish, index) => (
                            <li key={index}>
                              &#x2022;<span className="ml-2">{dish}</span>
                            </li>
                          ))}
                        </ul>
                      </div>
                      <div className="self-start flex items-center justify-center gap-4 ft-regular">
                        <div>
                          <div>
                            <strong>ABV:</strong> {beer.abv}
                          </div>
                        </div>
                        <div>
                          <div>
                            <strong>IBU:</strong> {beer.ibu}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    id="ingredients-container"
                    className="p-5 border-2 rounded-md shadow-md bg-slate-100"
                  >
                    <h3 className="text-center mb-4 font-bold text-indigo-main">
                      Ingredients
                    </h3>
                    <div className="flex flex-wrap justify-between gap-4">
                      <IngredientsList
                        ingredientType={beer.ingredients.malt}
                        imgSrc={malt}
                        name="Malt"
                      />
                      <IngredientsList
                        ingredientType={beer.ingredients.hops}
                        imgSrc={hop}
                        name="Hops"
                      />
                      <div>
                        <div className="flex mb-4 gap-2">
                          <img src={yeast} alt={yeast} className="w-5" />
                          <h3 className="text-orange">Yeast</h3>
                        </div>
                        <div>{beer.ingredients.yeast}</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </main>
          </div>
        ))}
      </>
    );
  return <></>;
}
