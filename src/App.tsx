import { store } from "../app/store.ts";
import { Provider } from "react-redux";
import {
  createBrowserRouter,
  Navigate,
  RouterProvider,
} from "react-router-dom";
import BeerDetails from "./views/BeerDetails.tsx";
import Home from "./views/Home";

function App(): JSX.Element {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Navigate replace to={"/beers?page=1"} />,
      errorElement: <div>Error</div>,
    },
    {
      path: "/beers",
      element: <Home />,
      errorElement: <div>Error</div>,
    },
    {
      path: "/details/:beerId",
      element: <BeerDetails />,
      errorElement: <div>Error</div>,
    },
  ]);
  return (
    <>
      <Provider store={store}>
        <RouterProvider router={router} />
      </Provider>
    </>
  );
}

export default App;
