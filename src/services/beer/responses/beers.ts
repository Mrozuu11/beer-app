type Amount = {
    value: number,
    unit: string
}

export type Ingredient = {
    name: string,
    amount: Amount,
    add?: string,
    attribute: string,
}

type Ingredients = {
    malt: Ingredient[],
    hops: Ingredient[],
    yeast: string,
}

type Method = {
    mash_temp: [
        {
            temp: Amount,
            duration: number
        }
    ],
    fermentation: {
        temp: Amount
    },
    twist: null
}

export type Beer = {
    id: string;
    name: string;
    tagline: string;
    first_brewed: string,
    description: string,
    image_url: string,
    abv: number,
    ibu: number,
    target_fg: number,
    target_og: number,
    ebc: number,
    srm: number,
    ph: number,
    attenuation_level: number,
    volume: Amount,
    boil_volume: Amount,
    method: Method,
    ingredients: Ingredients;
    food_pairing: string[],
    brewers_tips: string,
    contributed_by: string
}