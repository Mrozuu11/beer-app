import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { Beer } from './beer/responses/beers'


// Define a service using a base URL and expected endpoints
export const beerApi = createApi({
    reducerPath: 'beerApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'https://api.punkapi.com/v2/' }),
    endpoints: (builder) => ({
        getBeers: builder.query<Beer[], number>({
            query: (page = 1) => `beers?page=${page}&per_page=12`,
        }),
        getBeerById: builder.query<Beer[], string>({
            query: (id) => `beers/${id}`,
        })
    }),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetBeersQuery, useGetBeerByIdQuery, usePrefetch } = beerApi