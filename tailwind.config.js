/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,tsx}", "./src/**/*"],
  theme: {
    extend: {
      colors: {
        //indigo-500
        "indigo-main": "#6366f1",
        //orange-400
        orange: "#fb923c",
      },
    },
  },
  plugins: [],
};
