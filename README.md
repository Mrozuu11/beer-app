# beer-app

This App was created using to enable a simple beer check, the data comes from **PUNK API** and the UI was build using **React**

Tech Stack:

- **React**
- **TypeScript**
- **Vite**
- **React Router V6**
- **Tailwind CSS**
- **MaterialUI Icons**

## Getting Started

First, install dependencies and run the development server:

```bash
npm install
# and
npm start
# or
yarn start
# or
pnpm start
```

Open [http://localhost:5173](http://localhost:5173) with your browser to see the result.
